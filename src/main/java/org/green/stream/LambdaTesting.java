package org.green.stream;

import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


@Service
public class LambdaTesting {

    public void streamTest1() {
        String[] arr = {"Tom", "Jack", "Harry"};
        Stream<String> arrStream = Stream.of(arr);

        arrStream.map(e -> e.toUpperCase()).forEach(s -> System.out.println(s));
    }

    public void streamTest2() {
        IntStream.range(1, 10).forEach(System.out::println);
    }

    public void streamTest3() {
        IntStream
                .range(1, 10) // Print 1 to 10 values
                .skip(5) // Skip the first 5 elements
                .forEach(x -> System.out.print(x));
    }

    public void streamTest4() {
        System.out.println(
                IntStream
                        .range(1, 5) // Print 1 to 5 values
                        .sum() // sum of all from the range above.
        );
    }

    public void streamTest5() {
        Stream
                .of("averi", "aneri", "alberto") // arguments
                .sorted() // sort the values
                .findFirst() // Find the first item
                .ifPresent(System.out::println); // Print the first iterm in the list
    }

    public void streamTest6() {
        Stream
                .of("averi", "aneri", "alberto", "Sarika", "Santhosh", "Cabonara") // Stream of names
                .filter(x -> x.startsWith("S")) // Filter which starts with S
                .forEach(System.out::println); // Print the results of each
    }

    public void streamTest7() {
        Arrays
                .stream(new int[]{2, 4, 6, 8, 10})
                .map(x -> x * x) // squares
                .average() // average of squares
                .ifPresent(System.out::println);
    }

    public void streamTest8() {
        List<String> people = Arrays.asList("Al", "Brent", "Sarika", "amanda", "Hans", "Shivi");
        people
                .stream()
                .map(String::toLowerCase) //
                .filter(x -> x.startsWith("a")) // x items which starts with a
                .forEach(System.out::println);
    }

    public void streamTest9() throws Exception {
        Path path = Paths.get(getClass().getClassLoader()
                .getResource("bands.txt").toURI());

        Stream<String> lines = Files.lines(path);
        lines.filter(x -> x.contains("an")).collect(Collectors.toList()).forEach(System.out::println);
        lines.close();
    }

    public void streamTest10() throws Exception {
        Path path = Paths.get(getClass().getClassLoader()
                .getResource("bands.txt").toURI());

        List<String> people = Files.lines(path).filter(x -> x.contains("an")).collect(Collectors.toList());
        people.forEach(System.out::println);
    }

    public void streamTest11() throws Exception {
        Path path = Paths.get(getClass().getClassLoader()
                .getResource("data.txt").toURI());

        Stream<String> rows = Files.lines(path);
        int rowCount = (int) rows
                .map(x -> x.split(",")) // sprint array at ,
                .filter(x -> x.length == 3) // filter out records which doesn't have length of 5.
                .count();
        System.out.println(rowCount + "");
        rows.close();
    }

    public void streamTest12() throws Exception {
        Path path = Paths.get(getClass().getClassLoader()
                .getResource("data.txt").toURI());

        Stream<String> rows = Files.lines(path);
        rows
                .map(x -> x.split(",")) // sprint array at ,
                .filter(x -> x.length == 3) // filter out records which doesn't have length of 5.
                .filter(x -> Integer.parseInt(x[1]) > 15) // filter out records which doesn't have length of 5.
                .forEach(x -> System.out.println(x[0] + " " + x[1] + " " + x[2]));
        rows.close();
    }

    public void streamTest13() throws Exception {
        Path path = Paths.get(getClass().getClassLoader()
                .getResource("data.txt").toURI());

        Stream<String> rows = Files.lines(path);
        rows
                .map(x -> x.split(",")) // sprint array at ,
                .filter(x -> x.length == 3) // filter out records which doesn't have length of 5.
                .filter(x -> Integer.parseInt(x[1]) > 15) // filter out records which doesn't have length of 5.
                .collect(Collectors.toMap(
                        x -> x[0],
                        x -> Integer.parseInt(x[1])
                )).forEach((s, integer) -> System.out.println("value : "+s + ", " + integer));
        rows.close();
    }

    public void streamTest14() {
        List<String> people = Arrays.asList("Al", "Brent", "Sarika", "amanda", "Hans", "Shivi");
        List<String> filteredPeople = people.stream().map(String::toLowerCase) //
                                            .filter(x -> x.startsWith("a") && x.endsWith("a")) // x items which starts with a
                                            .collect(Collectors.toList());

        System.err.println(filteredPeople.toString());
    }

    public void methodCall() {
        System.err.println("Dynamic method call works!!!");
    }
}
