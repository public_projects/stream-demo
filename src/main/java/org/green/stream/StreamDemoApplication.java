package org.green.stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;


@SpringBootApplication
public class StreamDemoApplication implements CommandLineRunner {

	@Autowired
	private LambdaTesting lambdaTesting;

	@Override
	public void run(String... args) throws Exception {
		try {
			Arrays.stream(lambdaTesting.getClass().getMethods())
				  .filter(x -> x.getName().contains("Test"))
				  .map(x -> x.getName())
				  .sorted(Comparator.comparingInt(x -> {
					  return Integer.parseInt(x.substring(x.lastIndexOf("Test") + 4, x.length()));
				  }))
				  .collect(Collectors.toList()).
													   forEach(methodName -> {
																   try {
																	   System.out.println();
																	   System.out.println("---------------------------");
																	   System.out.println(methodName);
																	   System.out.println("---------------------------");
																	   lambdaTesting.getClass().getMethod(methodName).invoke(lambdaTesting, null);
																	   System.out.println();
																   } catch (Exception ex) {
																	   ex.printStackTrace();
																   }
															   }
															  );

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("");
		System.out.println("-------------------------------");
		System.out.println("--------------------");
	}

	public static void main(String[] args) {
		SpringApplication.run(StreamDemoApplication.class, args);
	}

}
